module.exports = function (grunt) {

  grunt.initConfig({

    jshint: {
      all: ['public/js/*.js']
    },

    uglify: {
      build: {
        options: {
          sourceMap : true
        },
        files: {
          'public/dist/js/app.min.js': ['public/js/*.js']
        }
      }
    },

    copy: {
      main: {
        files: [
          { cwd: 'public/libs', src: ['*min.js', '*.js.map'], dest: 'public/dist/js', expand: true },
        ]
      }
    },

    less: {
      build: {
        files: {
          'public/dist/css/style.css': 'public/css/style.less'
        }
      }
    },

    cssmin: {
      build: {
        files: {
          'public/dist/css/style.min.css': 'public/dist/css/style.css',
          'public/dist/css/signin.min.css': 'public/css/signin.css'
        }
      }
    },

    watch: {
      css: {
        files: ['public/css/*.less'],
        tasks: ['less', 'cssmin']
      },
      js: {
        files: ['public/js/*.js'],
        tasks: ['jshint', 'uglify']
      }
    },

    nodemon: {
      dev: {
        script: 'server.js'
      }
    },

    concurrent: {
      options: {
        logConcurrentOutput: true
      },
      tasks: ['nodemon', 'watch']
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-nodemon');
  grunt.loadNpmTasks('grunt-concurrent');

  grunt.registerTask('default', ['less', 'cssmin', 'jshint', 'uglify', 'copy', 'concurrent']);
};