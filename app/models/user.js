'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  crypto = require('crypto');

/** Validate input */
var validate = function (input) {
  return ((this.provider !== 'local' && !this.updated) || property.length);
};

/** Password validation that i skipped for local development */
var validatePassword = function (password) {
  return (this.provider !== 'local' || (password && password.length > 6));
};

var UserSchema = new Schema({
  email: {
    type: String,
    trim: true,
    default: '',
    validate: [validate, 'Please enter your email'],
    match: [/.+\@.+\..+/, 'Please enter a valid email address']
  },
  username: {
    type: String,
    unique: true,
    required: 'Please fill in a username',
    trim: true
  },
  password: {
    type: String,
    default: '',
    validate: [validatePassword, 'You password must be at least six characters long']
  },
  salt: {
    type: String
  },
  provider: {
    type: String,
    required: 'Provider is required'
  },
  updated: {
    type: Date
  },
  created: {
    type: Date,
    default: Date.now
  }
});

/** Hook a pre save method to hash the password */
UserSchema.pre('save', function(next) {
  if (this.password && this.password.length > 6) {
    this.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
    this.password = this.hashPassword(this.password);
  }
  next();
});

/** Create instance method for authenticating user */
UserSchema.methods.authenticate = function(password) {
  return this.password === this.hashPassword(password);
};

/** Find possible not used username */
UserSchema.statics.findUniqueUsername = function(username, suffix, callback) {
  var _this = this;
  var possibleUsername = username + (suffix || '');

  _this.findOne({
    username: possibleUsername
  }, function(err, user) {
    if (!err) {
      if (!user) {
        callback(possibleUsername);
      } else {
        return _this.findUniqueUsername(username, (suffix || 0) + 1, callback);
      }
    } else {
      callback(null);
    }
  });
};

mongoose.model('User', UserSchema);