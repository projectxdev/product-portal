angular.module('muse.controllers', [])

  .controller('MainCtrl', [ '$rootScope', '$scope', '$http', '$location', function ( $rootScope, $scope, $http, $location) {
    'use strict';

    $scope.first = true;

    var userRequestData = { loginUser: $rootScope.loggedInUser, loginPw : $rootScope.loggedInPw };
    $http.post('/auth', userRequestData )
      .success(function(data) {
      })
      .error( function(data) {
      } );

    $scope.logout = function() {
      $rootScope.loggedInUser = null;
      $rootScope.loggedInPw = null;
    };
  }])

  .controller('RootCtrl', [ '$rootScope', '$scope', '$http', 'md5', '$route', function ( $rootScope, $scope, $http, md5, $route) {
    'use strict';

    $rootScope.loggedInUser = null;
    $rootScope.loggedInPw = null;
    $rootScope.loggedIn = false;

    $scope.logInFailed = false;
    $scope.loginUser = '';
    $scope.loginPw = '';

    $scope.loginAttempt = function() {

      var hashedUser =  md5.createHash($scope.loginUser || '');
      var hashedPw = md5.createHash($scope.loginPw || '');
      var userRequestData = { loginUser: hashedUser, loginPw : hashedPw };
      $http.post('/login', userRequestData )
        .success(function(data) {

          $scope.logInFailed = false;

          $rootScope.loggedInUser = hashedUser;
          $rootScope.loggedInPw = hashedPw;
          $rootScope.loggedIn = true;
      })
        .error( function(data) {
          $scope.logInFailed = true;
      } );
   };
}]);
