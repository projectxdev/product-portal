var app = angular.module('muse', [
  'ngRoute',
  'angular-md5',
  'muse.controllers'
]);

app.config(['$routeProvider', function ($routeProvider) {
  'use strict';

  $routeProvider
    .when('/', {
      controller: 'RootCtrl',
      templateUrl: 'login.html',
      resolve: {
        init: ['$rootScope', function ($rootScope) {
          $rootScope.context = 'RootCtrl';
        }]
      }
    })
    .when('/main', {
      controller: 'MainCtrl',
      templateUrl: 'main.html',
      resolve: {
        init: ['$rootScope', function ($rootScope) {
          $rootScope.context = 'MainCtrl';
        }]
      }
    })
    .when('/user', {
      controller: 'UserCtrl',
      templateUrl: 'user.html',
      resolve: {
        init: ['$rootScope', function ($rootScope) {
          $rootScope.context = 'UserCtrl';
        }]
      }
    })
    .otherwise({redirectTo: '/'});
  }]);

app.run( function($rootScope, $location) {
  $rootScope.$on('$routeChangeSuccess', function () {
    if ($rootScope.loggedInUser === null)
      $location.url("/");
  });
});